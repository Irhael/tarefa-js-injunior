const converterCelsius = (f) => {
    return (5 * (f-32)/9)
}

let tempf = Math.round(Math.random()*(100));
console.log(tempf +"°Fahrenheit = " + converterCelsius(tempf) + "°Celsius.");
